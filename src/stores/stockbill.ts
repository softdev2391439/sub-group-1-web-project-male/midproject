
import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'

//import store
import { useMaterialStore } from './material';
import { useUserStore } from './user'

//import type
import type { StockBill } from '@/types/StockBill';
import type { StockBillDetail } from '@/types/StockBillDetail';
import type { Material } from '@/types/Material';

const userStore = useUserStore();
const materialStore = useMaterialStore();

const amount = ref(0);
const price =ref(0)
const invDialog = ref(false)


export const useStockBillStore = defineStore('stockBill ', () => {
    const initStockBill = ref<StockBill>({
        id: 0,
        creatDate: new Date(),
        total: 0,
        totalItem: 0,
        userID: userStore.currentUser?.id ?? 0,
        paid:0,
        stockBillDeatils: []
    })
    
    const stockBills = ref<StockBill[]>([
        {
            id: 0,
            creatDate: new Date('2023-12-01'),
            total: 100,
            totalItem: 20,
            userID: 2,
            paid:50,
            stockBillDeatils: [
                { id: -22, name: "I", price: 0, amount: 0 ,material: {id:-1, name: "I",amount: 0 ,type:"Human" , minimum: 0}},
                { id: -1, name: "need", price: 0, amount: 0 ,material:  {id:-2, name: "need",amount: 0 ,type:"Human" , minimum: 0}},
                { id: -2, name: "to sleep", price: 0, amount: 0 ,material: {id:-2, name: "to sleep",amount: 0 ,type:"Human" , minimum: 0}}
            ]

        },
        {
            id: -3,
            creatDate: new Date('2023-12-06'),
            total: 100,
            totalItem: 20,
            userID: 3,
            paid:50,
            stockBillDeatils: [
                { id: -22, name: "W", price: 0, amount: 0 ,material: {id:-1, name: "I",amount: 0 ,type:"Human" , minimum: 0}},
                { id: -1, name: "T", price: 0, amount: 0 ,material:  {id:-2, name: "need",amount: 0 ,type:"Human" , minimum: 0}},
                { id: -2, name: "Pass", price: 0, amount: 0 ,material: {id:-2, name: "to sleep",amount: 0 ,type:"Human" , minimum: 0}}
            ]

        }
    ])

    const stockBillDetails = ref<StockBillDetail[]>([
    ])
    

    const addItem = (material: Material) => {
        console.log(material)
        const index = stockBillDetails.value.findIndex((item) => item.material?.id === material.id)
        console.log(index)
        if (index >= 0) {
            stockBillDetails.value[index].amount++
            calBill()
            return
        } else {
            const newItem: StockBillDetail = {
                id: stockBillDetails.value.length + 1,
                name: material.name,
                price: 0,
                amount: 0,
                material: material
            }
            updateValue(newItem)
            nextTick(() => {
                stockBillDetails.value.push(newItem)
                calBill()
                clearVaule()
            })
           
            
        }
    }

    function updateValue(item:any){
        item.amount =amount.value;
        item.price =price.value;

    }

    function calBill() {
        let total = 0
        let totalItem = 0
        for (const item of stockBillDetails.value) {
            total = total + (item.price * item.amount)
            totalItem = Number(totalItem) + Number(item.amount);
        }
        initStockBill.value.totalItem = totalItem
        initStockBill.value.total = total
        initStockBill.value.stockBillDeatils = stockBillDetails.value

    }

    function saveBill(){
        initStockBill.value.id= stockBills.value.length + 1;
        console.log(initStockBill.value)
        stockBills.value.push(initStockBill.value)
        clear()
    }

    const deleteItem = (m: StockBillDetail) => {
        console.log("item",m)
        const index = stockBillDetails.value.findIndex((item) => item.id === m.id)
        console.log("Item  that find ",stockBillDetails.value.find((item) => item.id === item.id))
        console.log("Index",index)
        stockBillDetails.value.splice(index, 1)
        calBill()
    }

    function clear(){
        stockBillDetails.value = []
        initStockBill.value = {
            id: 0,
            creatDate: new Date(),
            total: 0,
            totalItem: 0,
            userID: userStore.currentUser?.id ?? 0,
            paid:0,
            stockBillDeatils: []
          
        }
        clearVaule()
        invDialog.value = false
      }

      function clearVaule(){
        amount.value = 0
        price.value = 0
      }

      function getUserName(userID: any) {
        const user = userStore.users.find((user) => user && user.id === userID);
        return user ? user.fullName : 'Unknown User';
        console.log(user);
      }

    return {
        initStockBill, stockBillDetails, addItem, calBill, deleteItem,saveBill,stockBills,amount,price,clear,invDialog,getUserName
    };



})