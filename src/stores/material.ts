import { ref, computed, nextTick } from 'vue';
import { defineStore } from 'pinia'

import type { Material } from '@/types/Material'
export const useMaterialStore = defineStore('materialStore', () => {
  const dialog = ref(false);
  const dialogDelete = ref(false);

  const initMaterial: Material = {
    id: -1,
    name:"",
    type: "",
    minimum: 0,
    amount: 0
  }
  const editMaterial = ref<Material>(JSON.parse(JSON.stringify(initMaterial)));
  const materials = ref<Material[]>([
    { id: 1, name: "เมล็ดกาแฟ", type: "กิโลกรัม", minimum: 10, amount: 15 },
    { id: 2, name: "เมล็ดโกโก้", type: "กิโลกรัม", minimum: 10, amount: 12 },
    { id: 3, name: "นมสด", type: "ลิตร", minimum: 10, amount: 18 },
    { id: 4, name: "ช็อกโกแลต", type: "กิโลกรัม", minimum: 10, amount: 8 },
    { id: 5, name: "น้ำตาลทราย", type: "กิโลกรัม", minimum: 10, amount: 13 },
    { id: 6, name: "วิปครีม", type: "ลิตร", minimum: 10, amount: 17 },
    { id: 7, name: "สตรอเบอรี่", type: "กิโลกรัม", minimum: 10, amount: 9 },
    { id: 8, name: "เบคอน", type: "กิโลกรัม", minimum: 10, amount: 14 },
    { id: 9, name: "ผงโกโก้", type: "กิโลกรัม", minimum: 10, amount: 11 },
    { id: 10, name: "เนสกาแฟ", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 11, name: "น้ำมะเขือเทศ", type: "ลิตร", minimum: 10, amount: 7 },
    { id: 12, name: "ผงฟู", type: "กิโลกรัม", minimum: 10, amount: 19 },
    { id: 13, name: "สับปะรด", type: "กิโลกรัม", minimum: 10, amount: 6 },
    { id: 14, name: "มะม่วงสด", type: "กิโลกรัม", minimum: 10, amount: 10 },
    { id: 15, name: "ผงครีมออร์แกนิค", type: "กิโลกรัม", minimum: 10, amount: 15 },
    { id: 16, name: "น้ำมันมะพร้าว", type: "ลิตร", minimum: 10, amount: 8 },
    { id: 17, name: "ชาเขียว", type: "กิโลกรัม", minimum: 10, amount: 11 },
    { id: 18, name: "มิ้นท์", type: "กิโลกรัม", minimum: 10, amount: 14 },
    { id: 19, name: "ผงวิปครีม", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 20, name: "เมล็ดข้าวโอ๊ต", type: "กิโลกรัม", minimum: 10, amount: 9 },
    { id: 21, name: "เมล็ดถั่วลิสง", type: "กิโลกรัม", minimum: 10, amount: 13 },
    { id: 22, name: "มะเขือเทศแช่แข็ง", type: "กิโลกรัม", minimum: 10, amount: 18 },
    { id: 23, name: "เนยสด", type: "กิโลกรัม", minimum: 10, amount: 7 },
    { id: 24, name: "สีผสมอาหาร", type: "กิโลกรัม", minimum: 10, amount: 19 },
    { id: 25, name: "น้ำมะนาว", type: "ลิตร", minimum: 10, amount: 12 },
    { id: 26, name: "กลิ่นวานิลลา", type: "ลิตร", minimum: 10, amount: 15 },
    { id: 27, name: "น้ำมันรำข้าว", type: "ลิตร", minimum: 10, amount: 11 },
    { id: 28, name: "เมล็ดแฟลกซีด", type: "กิโลกรัม", minimum: 10, amount: 17 },
    { id: 29, name: "เมล็ดมะม่วงหิมพานต์", type: "กิโลกรัม", minimum: 10, amount: 8 },
    { id: 30, name: "ผงปรุงรสวนิลลา", type: "กิโลกรัม", minimum: 10, amount: 13 },
    { id: 31, name: "เกลือ", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 32, name: "น้ำมันวนิลลา", type: "ลิตร", minimum: 10, amount: 9 },
    { id: 33, name: "ผงกาแฟ", type: "กิโลกรัม", minimum: 10, amount: 14 },
    { id: 34, name: "เมล็ดเมล่อน", type: "กิโลกรัม", minimum: 10, amount: 15 },
    { id: 35, name: "น้ำมันโอลีฟ", type: "ลิตร", minimum: 10, amount: 6 },
    { id: 36, name: "ผงวนิลลา", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 37, name: "กลูโคส", type: "กิโลกรัม", minimum: 10, amount: 17 },
    { id: 38, name: "น้ำมันอาหาร", type: "ลิตร", minimum: 10, amount: 12 },
    { id: 39, name: "ผงไข่", type: "กิโลกรัม", minimum: 10, amount: 19 },
    { id: 40, name: "น้ำมันถั่วเหลือง", type: "ลิตร", minimum: 10, amount: 7 },
    { id: 41, name: "พริกไทย", type: "กิโลกรัม", minimum: 10, amount: 8 },
    { id: 42, name: "น้ำมันหอยนางรม", type: "ลิตร", minimum: 10, amount: 14 },
    { id: 43, name: "เกลือป่น", type: "กิโลกรัม", minimum: 10, amount: 12 },
    { id: 44, name: "ผงหมู", type: "กิโลกรัม", minimum: 10, amount: 7 },
    { id: 45, name: "น้ำมันทานตะวัน", type: "ลิตร", minimum: 10, amount: 13 },
    { id: 46, name: "เมล็ดสาลีอเนกประสงค์", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 47, name: "ผงพิมพ์", type: "กิโลกรัม", minimum: 10, amount: 18 },
    { id: 48, name: "น้ำมันงา", type: "ลิตร", minimum: 10, amount: 9 },
    { id: 49, name: "เมล็ดมะม่วงฝน", type: "กิโลกรัม", minimum: 10, amount: 11 },
    { id: 50, name: "น้ำมันหอมระเหย", type: "ลิตร", minimum: 10, amount: 15 },
  ]
  )

  let editedIndex = -1;
  
  function closeDelete() {
    dialogDelete.value = false;
    nextTick(() => {
      editMaterial.value = Object.assign({}, initMaterial);
    });
  }

  function deleteItemConfirm() {
    materials.value.splice(editedIndex, 1);
    closeDelete();
  }

  function editItem(item: Material) {
    editedIndex = materials.value.indexOf(item);
    editMaterial.value = Object.assign({}, item);
    dialog.value = true;
  }

  function deleteItem(item: Material) {
    editedIndex =materials.value.indexOf(item);
    editMaterial.value = Object.assign({}, item);
    dialogDelete.value = true;
  }


  function initialize(){
    materials.value = ([
    { id: 1, name: "เมล็ดกาแฟ", type: "กิโลกรัม", minimum: 10, amount: 15 },
    { id: 2, name: "เมล็ดโกโก้", type: "กิโลกรัม", minimum: 10, amount: 12 },
    { id: 3, name: "นมสด", type: "ลิตร", minimum: 10, amount: 18 },
    { id: 4, name: "ช็อกโกแลต", type: "กิโลกรัม", minimum: 10, amount: 8 },
    { id: 5, name: "น้ำตาลทราย", type: "กิโลกรัม", minimum: 10, amount: 13 },
    { id: 6, name: "วิปครีม", type: "ลิตร", minimum: 10, amount: 17 },
    { id: 7, name: "สตรอเบอรี่", type: "กิโลกรัม", minimum: 10, amount: 9 },
    { id: 8, name: "เบคอน", type: "กิโลกรัม", minimum: 10, amount: 14 },
    { id: 9, name: "ผงโกโก้", type: "กิโลกรัม", minimum: 10, amount: 11 },
    { id: 10, name: "เนสกาแฟ", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 11, name: "น้ำมะเขือเทศ", type: "ลิตร", minimum: 10, amount: 7 },
    { id: 12, name: "ผงฟู", type: "กิโลกรัม", minimum: 10, amount: 19 },
    { id: 13, name: "สับปะรด", type: "กิโลกรัม", minimum: 10, amount: 6 },
    { id: 14, name: "มะม่วงสด", type: "กิโลกรัม", minimum: 10, amount: 10 },
    { id: 15, name: "ผงครีมออร์แกนิค", type: "กิโลกรัม", minimum: 10, amount: 15 },
    { id: 16, name: "น้ำมันมะพร้าว", type: "ลิตร", minimum: 10, amount: 8 },
    { id: 17, name: "ชาเขียว", type: "กิโลกรัม", minimum: 10, amount: 11 },
    { id: 18, name: "มิ้นท์", type: "กิโลกรัม", minimum: 10, amount: 14 },
    { id: 19, name: "ผงวิปครีม", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 20, name: "เมล็ดข้าวโอ๊ต", type: "กิโลกรัม", minimum: 10, amount: 9 },
    { id: 21, name: "เมล็ดถั่วลิสง", type: "กิโลกรัม", minimum: 10, amount: 13 },
    { id: 22, name: "มะเขือเทศแช่แข็ง", type: "กิโลกรัม", minimum: 10, amount: 18 },
    { id: 23, name: "เนยสด", type: "กิโลกรัม", minimum: 10, amount: 7 },
    { id: 24, name: "สีผสมอาหาร", type: "กิโลกรัม", minimum: 10, amount: 19 },
    { id: 25, name: "น้ำมะนาว", type: "ลิตร", minimum: 10, amount: 12 },
    { id: 26, name: "กลิ่นวานิลลา", type: "ลิตร", minimum: 10, amount: 15 },
    { id: 27, name: "น้ำมันรำข้าว", type: "ลิตร", minimum: 10, amount: 11 },
    { id: 28, name: "เมล็ดแฟลกซีด", type: "กิโลกรัม", minimum: 10, amount: 17 },
    { id: 29, name: "เมล็ดมะม่วงหิมพานต์", type: "กิโลกรัม", minimum: 10, amount: 8 },
    { id: 30, name: "ผงปรุงรสวนิลลา", type: "กิโลกรัม", minimum: 10, amount: 13 },
    { id: 31, name: "เกลือ", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 32, name: "น้ำมันวนิลลา", type: "ลิตร", minimum: 10, amount: 9 },
    { id: 33, name: "ผงกาแฟ", type: "กิโลกรัม", minimum: 10, amount: 14 },
    { id: 34, name: "เมล็ดเมล่อน", type: "กิโลกรัม", minimum: 10, amount: 15 },
    { id: 35, name: "น้ำมันโอลีฟ", type: "ลิตร", minimum: 10, amount: 6 },
    { id: 36, name: "ผงวนิลลา", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 37, name: "กลูโคส", type: "กิโลกรัม", minimum: 10, amount: 17 },
    { id: 38, name: "น้ำมันอาหาร", type: "ลิตร", minimum: 10, amount: 12 },
    { id: 39, name: "ผงไข่", type: "กิโลกรัม", minimum: 10, amount: 19 },
    { id: 40, name: "น้ำมันถั่วเหลือง", type: "ลิตร", minimum: 10, amount: 7 },
    { id: 41, name: "พริกไทย", type: "กิโลกรัม", minimum: 10, amount: 8 },
    { id: 42, name: "น้ำมันหอยนางรม", type: "ลิตร", minimum: 10, amount: 14 },
    { id: 43, name: "เกลือป่น", type: "กิโลกรัม", minimum: 10, amount: 12 },
    { id: 44, name: "ผงหมู", type: "กิโลกรัม", minimum: 10, amount: 7 },
    { id: 45, name: "น้ำมันทานตะวัน", type: "ลิตร", minimum: 10, amount: 13 },
    { id: 46, name: "เมล็ดสาลีอเนกประสงค์", type: "กิโลกรัม", minimum: 10, amount: 16 },
    { id: 47, name: "ผงพิมพ์", type: "กิโลกรัม", minimum: 10, amount: 18 },
    { id: 48, name: "น้ำมันงา", type: "ลิตร", minimum: 10, amount: 9 },
    { id: 49, name: "เมล็ดมะม่วงฝน", type: "กิโลกรัม", minimum: 10, amount: 11 },
    { id: 50, name: "น้ำมันหอมระเหย", type: "ลิตร", minimum: 10, amount: 15 },
  ]
  )

  }

   return { materials,dialog,dialogDelete,initMaterial,editMaterial,editedIndex,deleteItemConfirm,editItem,deleteItem,closeDelete,initialize }
})
