import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import type { CheckDate, CheckUser } from '@/types/CheckInOut/CheckInOutDate'

export const useCheckInOutStore = defineStore('checkInOut', () => {
    
    const checkDates = ref<CheckDate[]>([
        {
          id: 1,
          date: new Date('2023-12-31'),
          checkUser: [
            { id: 1, name: 'กฤติน ศรสุข', timeIn: '09:01', timeOut: '15:00',totalMin: 0 },
            { id: 2, name: 'สุดใจ มีความสุข', timeIn: '09:03', timeOut: '15:30',totalMin: 0 },
            { id: 3, name: 'รพีพัฒน์ ดีใจ', timeIn: '09:04', timeOut: '15:30',totalMin: 0 },
            { id: 4, name: 'ณัฐฐิชา พุฒิพงษ์', timeIn: '09:07', timeOut: '15:30',totalMin: 0 },
            { id: 5, name: 'ธีระพล ประดิษฐ์', timeIn: '09:12', timeOut: '15:30',totalMin: 0 },
            { id: 6, name: 'อรวรรณ สุขสวัสดิ์', timeIn: '09:18', timeOut: '15:30',totalMin: 0 },
            { id: 7, name: 'พิชาธิป ช่างมัน', timeIn: '09:22', timeOut: '15:30',totalMin: 0 },
            { id: 8, name: 'เจนนิ เดนเวอร์', timeIn: '09:22', timeOut: '15:30',totalMin: 0 },
            { id: 9, name: 'เอ็มมา สวัสดิ์ดี', timeIn: '09:24', timeOut: '15:30',totalMin: 0 },
            { id: 10, name: 'นัทธิพร วงศ์วารี', timeIn: '09:29', timeOut: '15:30',totalMin: 0 },
            { id: 11, name: 'ไอริส แซนเจอร์', timeIn: '09:37', timeOut: '15:30',totalMin: 0 },
            { id: 12, name: 'โบว์ เวิร์ธ', timeIn: '09:45', timeOut: '15:30',totalMin: 0 },
            { id: 13, name: 'แอลเล็กซ์ ซันเดอร์', timeIn: '09:45', timeOut: '15:30',totalMin: 0 },
            { id: 14, name: 'โมจิ มาซากิ', timeIn: '09:48', timeOut: '15:30',totalMin: 0 },
            { id: 15, name: 'แชร์ล็อต สตีเวนสัน', timeIn: '09:59', timeOut: '15:30',totalMin: 0 }
          ]
        },
        {
          id: 2,
          date: new Date('2024-01-01'),
          checkUser: [
            { id: 1, name: 'กฤติน ศรสุข', timeIn: '09:30', timeOut: '15:00',totalMin: 0 },
            { id: 2, name: 'สุดใจ มีความสุข', timeIn: '09:30', timeOut: '15:30',totalMin: 0 },
            { id: 3, name: 'รพีพัฒน์ ดีใจ', timeIn: '09:31', timeOut: '15:30',totalMin: 0 },
            { id: 4, name: 'ณัฐฐิชา พุฒิพงษ์', timeIn: '09:31', timeOut: '15:30',totalMin: 0 },
            { id: 5, name: 'ธีระพล ประดิษฐ์', timeIn: '09:33', timeOut: '15:30',totalMin: 0 },
            { id: 6, name: 'อรวรรณ สุขสวัสดิ์', timeIn: '09:37', timeOut: '15:30',totalMin: 0 },
            { id: 7, name: 'พิชาธิป ช่างมัน', timeIn: '09:40', timeOut: '15:30',totalMin: 0 },
            { id: 8, name: 'เจนนิ เดนเวอร์', timeIn: '09:45', timeOut: '15:30',totalMin: 0 },
            { id: 9, name: 'เอ็มมา สวัสดิ์ดี', timeIn: '09:47', timeOut: '15:30',totalMin: 0 },
            { id: 10, name: 'นัทธิพร วงศ์วารี', timeIn: '09:48', timeOut: '15:30',totalMin: 0 },
            { id: 11, name: 'ไอริส แซนเจอร์', timeIn: '09:50', timeOut: '15:30',totalMin: 0 },
            { id: 12, name: 'โบว์ เวิร์ธ', timeIn: '09:52', timeOut: '15:30',totalMin: 0 },
            { id: 13, name: 'แอลเล็กซ์ ซันเดอร์', timeIn: '09:58', timeOut: '15:30',totalMin: 0 },
            { id: 14, name: 'โมจิ มาซากิ', timeIn: '09:59', timeOut: '15:30',totalMin: 0 },
            { id: 15, name: 'แชร์ล็อต สตีเวนสัน', timeIn: '10:01', timeOut: '15:30',totalMin: 0 }
          ]
        },
        {
          id: 3,
          date: new Date('2024-01-10'),
          checkUser: [
            { id: 1, name: 'กฤติน ศรสุข', timeIn: '09:18', timeOut: '',totalMin: 0 },
            { id: 2, name: 'สุดใจ มีความสุข', timeIn: '09:24', timeOut: '',totalMin: 0 }
          ]
        },
        {
          id: 4,
          date: new Date('2024-01-12'),
          checkUser: [
            { id: 1, name: 'กฤติน ศรสุข', timeIn: '09:18', timeOut: '',totalMin: 0 },
            { id: 2, name: 'สุดใจ มีความสุข', timeIn: '09:24', timeOut: '',totalMin: 0 }
          ]
        }
      ]);

      let lastCheckDateId = checkDates.value.length+1
      

      const newCheckDate = {
        id: lastCheckDateId++,
        date: new Date(),
        checkUser: []
      }
      const newCheckUser = { id: 1, name: '', timeIn: '', timeOut: '',totalMin: 0 }

      function formattedTime(date:Date) {
        const hours = date.getHours().toString().padStart(2, '0');
        const minutes = date.getMinutes().toString().padStart(2, '0');
        const seconds = date.getSeconds().toString().padStart(2, '0');
      
        return `${hours}:${minutes}:${seconds}`;
      }
      
      const currentTime = new Date();
      const formatted = formattedTime(currentTime);
      console.log(formatted);  // เพื่อทดสอบ
      
      function formatDate(date:Date) {
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        return `${year}-${month}-${day}`;
      }
      function assignCheckUser(editedheckUser:CheckUser,id:number,name:string,date:Date) {
        editedheckUser.id=id
        editedheckUser.name=name
        editedheckUser.timeIn= formattedTime(date)
      }
      function assignTimeOutCheckUser(editedheckUser:CheckUser,date:Date) {
        editedheckUser.timeOut = formattedTime(date)
      }

      function calculateTotalMinutes(timeIn: string, timeOut: string): number {
        const convertToMinutes = (time: string): number => {
            const [hours, minutes] = time.split(':').map(Number);
            return hours * 60 + minutes;
        };
    
        return convertToMinutes(timeOut) - convertToMinutes(timeIn);
      }
    
    function updateTotalMinutes(checkDates: CheckDate[]): void {
        // ใช้ forEach เพื่อวนลูปผ่านทุกๆ checkDate และ checkUser ใน checkDates
        checkDates.forEach(checkDate => {
            checkDate.checkUser.forEach(checkUser => {
                // กำหนดค่า totalMin เป็น 0 ก่อน
                checkUser.totalMin = 0;
                // ให้คำนวณ totalMin และกำหนดค่าให้กับ checkUser
                checkUser.totalMin = calculateTotalMinutes(checkUser.timeIn, checkUser.timeOut);
            });
        });
    }
    function formatMinutesToHHMM(totalMinutes: number): string {
      const hours = Math.floor(totalMinutes / 60);
      const minutes = totalMinutes % 60;
      return `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}`;
    }

    const formatTime = (timeString: string) => {
      const parsedTime = new Date(`1970-01-01T${timeString}`);
      return parsedTime.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' });
    };
  
    

  return { checkDates,newCheckDate,newCheckUser,assignCheckUser,formattedTime,formatDate,lastCheckDateId,assignTimeOutCheckUser,calculateTotalMinutes,updateTotalMinutes,formatMinutesToHHMM,formatTime }
})