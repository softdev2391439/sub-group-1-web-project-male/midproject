import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'

//import store
import { useMaterialStore } from './material';
import { useUserStore } from './user'
//import type
import type { Stock } from '@/types/Stock'
import type { StockItem } from '@/types/StockItem'

import type { Material } from '@/types/Material';

export const useStockStore = defineStore('stockStore', () => {
  //set name of store
  const userStore = useUserStore()
  const materialStore = useMaterialStore()

  //set variable Dialog
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const editItemDialog = ref(false)
  const addNewMaterialDialog = ref(false)
  const StockDialog = ref(false)
  const saveMaterialDialog = ref(false)
  const addNewStockItemDialog = ref(false)
  const addSomeDialog = ref(false);
  const qoh = ref(0)

  const stock = ref<Stock>({
    id: 0,
    date: new Date(),
    userID: userStore.currentUser?.id ?? 0
  })

  const stocks = ref<Stock[]>([
    {
      id: 1,
      date: new Date('2023-12-01'),
      userID: 1,
      items: [
        { id: 1, QOH: 10, balance: 0, materialId: 1 },
        { id: 2, QOH: 5, balance: -5, materialId: 2 },
        { id: 3, QOH: 8, balance: -2, materialId: 3 },
        { id: 4, QOH: 15, balance: 5, materialId: 4 },
        { id: 5, QOH: 3, balance: -7, materialId: 5 },
      ]
    },
    {
      id: 2,
      date: new Date('2023-12-09'),
      userID: 2,
      items: [
        { id: 6, QOH: 12, balance: 2, materialId: 6 },
        { id: 7, QOH: 6, balance: -4, materialId: 7 },
        { id: 8, QOH: 9, balance: -1, materialId: 8 },
        { id: 9, QOH: 18, balance: 8, materialId: 9 },
        { id: 10, QOH: 4, balance: -6, materialId: 10 },
      ],
    }
  ])

  const initStockItem: StockItem = {
    id: -1,
    QOH: 0,
    balance: 0,
    materialId: -1
  }

  const stockItems = ref<StockItem[]>([

  ]);
  const editStockItem = ref<StockItem>(JSON.parse(JSON.stringify(initStockItem)))

  //for get value for ui to set new material
  const name = ref('');
  const minimum = ref(0);
  const typ = ref('');




  function addStockItem(stockItem: StockItem) {
    materialStore.materials.forEach((material, i) => {
      const newStockItem: StockItem = {
        id: i,
        QOH: 0,
        balance: 0,
        materialId: material.id
      };

      stockItems.value.push(newStockItem);
    });

    console.log(stockItems.value);
  }

  const addItem = (material: Material) => {
    console.log(material)
    const index = stockItems.value.findIndex((item) => item.materialId === material.id)
    console.log(index)
    if (index >= 0) {
      stockItems.value[index].QOH++
      return
    } else {
      const newItem: StockItem = {
        id: stockItems.value.length + 1,
        QOH: qoh.value,
        balance: qoh.value - material.minimum,
        materialId: material.id
      }

      nextTick(() => {
        stockItems.value.push(newItem)
        qoh.value=0

      })


    }
  }


  // Stockitems
  let editedIndex = -1

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editStockItem.value = Object.assign({}, initStockItem);
    })
  }

  function deleteItemConfirm() {
    const deletedItem = stockItems.value[editedIndex];

    if (deletedItem) {
      stockItems.value.splice(editedIndex, 1);

      const materialIndex = materialStore.materials.findIndex((mat) => mat.id === deletedItem.materialId);

      if (materialIndex !== -1) {
        materialStore.materials.splice(materialIndex, 1);
      }

      closeDelete();
    }
  }

  function editItem(item: StockItem) {
    editedIndex = stockItems.value.indexOf(item)
    editStockItem.value = { ...item }
    editItemDialog.value = true
  }
  function deleteItem(item: StockItem) {
    editedIndex = stockItems.value.findIndex((stoc) => stoc.materialId === item.materialId);
    editStockItem.value = Object.assign({}, item);
    dialogDelete.value = true;
  }

  const deleteItemSome = (m: StockItem) => {
    console.log("item",m)
    const index = stockItems.value.findIndex((item) => item.id === m.id)
    console.log("Item  that find ",stockItems.value.find((item) => item.id === item.id))
    console.log("Index",index)
    stockItems.value.splice(index, 1)
}

  function initializeItem() {
    stockItems.value = []
    
  }

  function getMaterialName(matrialId: any) {
    const material = materialStore.materials.find((mat) => mat && mat.id === matrialId);
    return material ? material.name : 'Unknown Material';
    console.log(material);
  }


  //updateQOHItems and creatStockItemPaper
  function updateQOHItems() {
    //updateQOH and call Balance
    addNewStockItemDialog.value = true
    for (const item of stockItems.value) {
      const updatedItem = stockItems.value.find((i) => i.id === item.id);
      if (updatedItem) {
        updatedItem.QOH = item.QOH;
        minimum.value = (materialStore.materials.find((mat) => mat.id === item.materialId)?.minimum ?? 0)
        updatedItem.balance = updatedItem.QOH - minimum.value;;

        const material = materialStore.materials.find((mat) => mat.id === item.materialId);
        if (material) {
          material.amount = item.QOH;
        }
      }
      ;
      console.log(stock.value);
    }

    //creatStockItemPaper
    stock.value.id = stocks.value.length + 1;
    stock.value.date = new Date();
    console.log("User", userStore.currentUser?.id ?? 0);
    stock.value.userID = userStore.currentUser?.id ?? 0;
    stock.value.items = stockItems.value;
    stocks.value.push(stock.value)
    console.log('Saving changes...', stockItems.value);
    stock.value = JSON.parse(JSON.stringify(stock))
    stockItems.value = []
    addNewStockItemDialog.value = false
  }

  //addnewMaterial item by StockItem
  function addNewMaterial() {
    console.log('name = ', name.value);
    console.log('manimum = ', minimum.value);
    console.log('type = ', typ.value);

    console.log("new save");
    console.log(materialStore.materials.length);
    const matID = materialStore.materials.length;
    editStockItem.value.id = stockItems.value.length;
    editStockItem.value.materialId = matID + 1;
    materialStore.editMaterial.id = matID + 1;
    materialStore.editMaterial.minimum = minimum.value;
    materialStore.editMaterial.name = name.value;
    materialStore.editMaterial.amount = editStockItem.value.QOH;
    materialStore.editMaterial.type = typ.value;

    console.log(editStockItem.value);
    console.log('material' + materialStore.editMaterial);

    stockItems.value.push(editStockItem.value);
    materialStore.materials.push(materialStore.editMaterial);

  }

  //stock
  function createCheckStock() {
    addNewStockItemDialog.value = true
    addStockItem(initStockItem)

  }
  function getUserName(userID: any) {
    const user = userStore.users.find((user) => user && user.id === userID);
    return user ? user.fullName : 'Unknown User';
    console.log(user);
  }

  function getMinimum(materialId: any) {
    const material = materialStore.materials.find((mat) => mat && mat.id === materialId);
    return material ? material.minimum : null;
    console.log(material);
  }


  function formatDate(date: Date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
  }
function close(){
  stockItems.value = []
  addNewStockItemDialog.value = false
  addSomeDialog.value = false

}




  return {
    stockItems, dialog, dialogDelete, addStockItem, editStockItem, editedIndex, deleteItemConfirm, editItem, deleteItem,
    closeDelete, initStockItem, initializeItem, editItemDialog, getMaterialName, updateQOHItems, addNewMaterial, typ, minimum,
    name, addNewMaterialDialog, createCheckStock, saveMaterialDialog, stocks, getUserName, formatDate, getMinimum, StockDialog,
    addNewStockItemDialog, addSomeDialog, qoh, addItem,deleteItemSome,close
  }
})
