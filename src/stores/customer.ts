import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Customer } from '@/types/Customer';
import type { VForm } from 'vuetify/components';

export const useCustomerStore = defineStore('customer', () => {


  const customers = ref<Customer[]>([
    {
      id: 1,
      name: 'อนุชิต มักข้อง',
      tel: '0817345840',
      gender: 'male',
      inDate: new Date('2020-03-01'),
      pointAmount: 20,
      pointRate: 0.05,
    },
    {
      id: 2,
      name: 'สมชาย ใจดี',
      tel: '0823456789',
      gender: 'female',
      inDate: new Date('2020-04-15'),
      pointAmount: 30,
      pointRate: 0.05,
    },
    {
      id: 3,
      name: 'วีระ สุขใจ',
      tel: '0834567890',
      gender: 'other',
      inDate: new Date('2020-05-22'),
      pointAmount: 25,
      pointRate: 0.05,
    },
    {
      id: 4,
      name: 'ประธาน รักชาติ',
      tel: '0845678901',
      gender: 'male',
      inDate: new Date('2020-06-10'),
      pointAmount: 15,
      pointRate: 0.05,
    },
    {
      id: 5,
      name: 'นิศา รักสันต์',
      tel: '0856789012',
      gender: 'female',
      inDate: new Date('2020-07-05'),
      pointAmount: 40,
      pointRate: 0.05,
    },
    {
      id: 6,
      name: 'กิตติพงศ์ บุญสุข',
      tel: '0867890123',
      gender: 'male',
      inDate: new Date('2020-08-18'),
      pointAmount: 18,
      pointRate: 0.05,
    },
    {
      id: 7,
      name: 'จริยา สุขศิลป์',
      tel: '0878901234',
      gender: 'other',
      inDate: new Date('2020-09-30'),
      pointAmount: 22,
      pointRate: 0.05,
    },
    {
      id: 8,
      name: 'สิริวัฒน์ รุ่งสว่าง',
      tel: '0889012345',
      gender: 'female',
      inDate: new Date('2020-10-12'),
      pointAmount: 35,
      pointRate: 0.05,
    },
    {
      id: 9,
      name: 'สุดใจ แสนเพราะ',
      tel: '0890123456',
      gender: 'male',
      inDate: new Date('2020-11-25'),
      pointAmount: 28,
      pointRate: 0.05,
    },
    {
      id: 10,
      name: 'รักชนะ ชนา',
      tel: '0901234567',
      gender: 'female',
      inDate: new Date('2020-12-08'),
      pointAmount: 32,
      pointRate: 0.05,
    },
    {
      id: 11,
      name: 'พัชราภรณ์ สุนทรางค์',
      tel: '0912345678',
      gender: 'other',
      inDate: new Date('2021-01-20'),
      pointAmount: 25,
      pointRate: 0.05,
    },
    {
      id: 12,
      name: 'พิมพ์ชนก สุรสิทธิ์',
      tel: '0923456789',
      gender: 'male',
      inDate: new Date('2021-02-05'),
      pointAmount: 15,
      pointRate: 0.05,
    },
    {
      id: 13,
      name: 'ปวีณา แสนสุข',
      tel: '0934567890',
      gender: 'female',
      inDate: new Date('2021-03-15'),
      pointAmount: 38,
      pointRate: 0.05,
    },
    {
      id: 14,
      name: 'ศุภวัฒน์ สุขสวัสดิ์',
      tel: '0945678901',
      gender: 'male',
      inDate: new Date('2021-04-28'),
      pointAmount: 20,
      pointRate: 0.05,
    },
    {
      id: 15,
      name: 'ณัฐนิตา นวลสุข',
      tel: '0956789012',
      gender: 'other',
      inDate: new Date('2021-05-10'),
      pointAmount: 28,
      pointRate: 0.05,
    },
    {
      id: 16,
      name: 'ศิริวัฒน์ พรหมสิริ',
      tel: '0967890123',
      gender: 'female',
      inDate: new Date('2021-06-22'),
      pointAmount: 42,
      pointRate: 0.05,
    },
    {
      id: 17,
      name: 'ภัทรพล รักวิวัฒน์',
      tel: '0978901234',
      gender: 'male',
      inDate: new Date('2021-07-07'),
      pointAmount: 24,
      pointRate: 0.05,
    },
    {
      id: 18,
      name: 'ศรัณย์วุฒิ มีดี',
      tel: '0989012345',
      gender: 'other',
      inDate: new Date('2021-08-18'),
      pointAmount: 30,
      pointRate: 0.05,
    },
    {
      id: 19,
      name: 'พลอยชมพู วงศ์สวัสดิ์',
      tel: '0990123456',
      gender: 'female',
      inDate: new Date('2021-09-30'),
      pointAmount: 36,
      pointRate: 0.05,
    },
    {
      id: 20,
      name: 'ธนบดี สุนทรางค์',
      tel: '0101234567',
      gender: 'male',
      inDate: new Date('2021-10-12'),
      pointAmount: 19,
      pointRate: 0.05,
    },
    {
      id: 21,
      name: 'ประพัฒน์ สุขสวัสดิ์',
      tel: '0112345678',
      gender: 'female',
      inDate: new Date('2021-11-25'),
      pointAmount: 28,
      pointRate: 0.05,
    },
    {
      id: 22,
      name: 'สุพัตรา นวลสุข',
      tel: '0123456789',
      gender: 'other',
      inDate: new Date('2021-12-08'),
      pointAmount: 22,
      pointRate: 0.05,
    },
    {
      id: 23,
      name: 'ธนกฤต รักชาติ',
      tel: '0134567890',
      gender: 'male',
      inDate: new Date('2022-01-20'),
      pointAmount: 15,
      pointRate: 0.05,
    },
    {
      id: 24,
      name: 'สุนิสา สุขสวัสดิ์',
      tel: '0145678901',
      gender: 'female',
      inDate: new Date('2022-02-05'),
      pointAmount: 33,
      pointRate: 0.05,
    },
    {
      id: 25,
      name: 'เทพวัฒน์ มีดี',
      tel: '0156789012',
      gender: 'male',
      inDate: new Date('2022-03-15'),
      pointAmount: 25,
      pointRate: 0.05,
    },
    {
      id: 26,
      name: 'ณัฐชนน แสนสุข',
      tel: '0167890123',
      gender: 'other',
      inDate: new Date('2022-04-28'),
      pointAmount: 40,
      pointRate: 0.05,
    },
    {
      id: 27,
      name: 'ปวริศ รักวิวัฒน์',
      tel: '0178901234',
      gender: 'female',
      inDate: new Date('2022-05-10'),
      pointAmount: 28,
      pointRate: 0.05,
    },
    {
      id: 28,
      name: 'ศุภกิจ พรหมสิริ',
      tel: '0189012345',
      gender: 'male',
      inDate: new Date('2022-06-22'),
      pointAmount: 22,
      pointRate: 0.05,
    },
    {
      id: 29,
      name: 'ศุภวิชญ์ พรหมสุข',
      tel: '0190123456',
      gender: 'other',
      inDate: new Date('2022-07-07'),
      pointAmount: 18,
      pointRate: 0.05,
    },
    {
      id: 30,
      name: 'รัชชานนท์ รักวิวัฒน์',
      tel: '0201234567',
      gender: 'female',
      inDate: new Date('2022-08-18'),
      pointAmount: 35,
      pointRate: 0.05,
    },
  ]);


  function initialize() {
    customers.value =
      [
        {
          id: 1,
          name: 'อนุชิต มักข้อง',
          tel: '0817345840',
          gender: 'male',
          inDate: new Date('2020-03-01'),
          pointAmount: 20,
          pointRate: 0.05,
        },
        {
          id: 2,
          name: 'สมชาย ใจดี',
          tel: '0823456789',
          gender: 'female',
          inDate: new Date('2020-04-15'),
          pointAmount: 30,
          pointRate: 0.05,
        },
        {
          id: 3,
          name: 'วีระ สุขใจ',
          tel: '0834567890',
          gender: 'other',
          inDate: new Date('2020-05-22'),
          pointAmount: 25,
          pointRate: 0.05,
        },
        {
          id: 4,
          name: 'ประธาน รักชาติ',
          tel: '0845678901',
          gender: 'male',
          inDate: new Date('2020-06-10'),
          pointAmount: 15,
          pointRate: 0.05,
        },
        {
          id: 5,
          name: 'นิศา รักสันต์',
          tel: '0856789012',
          gender: 'female',
          inDate: new Date('2020-07-05'),
          pointAmount: 40,
          pointRate: 0.05,
        },
        {
          id: 6,
          name: 'กิตติพงศ์ บุญสุข',
          tel: '0867890123',
          gender: 'male',
          inDate: new Date('2020-08-18'),
          pointAmount: 18,
          pointRate: 0.05,
        },
        {
          id: 7,
          name: 'จริยา สุขศิลป์',
          tel: '0878901234',
          gender: 'other',
          inDate: new Date('2020-09-30'),
          pointAmount: 22,
          pointRate: 0.05,
        },
        {
          id: 8,
          name: 'สิริวัฒน์ รุ่งสว่าง',
          tel: '0889012345',
          gender: 'female',
          inDate: new Date('2020-10-12'),
          pointAmount: 35,
          pointRate: 0.05,
        },
        {
          id: 9,
          name: 'สุดใจ แสนเพราะ',
          tel: '0890123456',
          gender: 'male',
          inDate: new Date('2020-11-25'),
          pointAmount: 28,
          pointRate: 0.05,
        },
        {
          id: 10,
          name: 'รักชนะ ชนา',
          tel: '0901234567',
          gender: 'female',
          inDate: new Date('2020-12-08'),
          pointAmount: 32,
          pointRate: 0.05,
        },
        {
          id: 11,
          name: 'พัชราภรณ์ สุนทรางค์',
          tel: '0912345678',
          gender: 'other',
          inDate: new Date('2021-01-20'),
          pointAmount: 25,
          pointRate: 0.05,
        },
        {
          id: 12,
          name: 'พิมพ์ชนก สุรสิทธิ์',
          tel: '0923456789',
          gender: 'male',
          inDate: new Date('2021-02-05'),
          pointAmount: 15,
          pointRate: 0.05,
        },
        {
          id: 13,
          name: 'ปวีณา แสนสุข',
          tel: '0934567890',
          gender: 'female',
          inDate: new Date('2021-03-15'),
          pointAmount: 38,
          pointRate: 0.05,
        },
        {
          id: 14,
          name: 'ศุภวัฒน์ สุขสวัสดิ์',
          tel: '0945678901',
          gender: 'male',
          inDate: new Date('2021-04-28'),
          pointAmount: 20,
          pointRate: 0.05,
        },
        {
          id: 15,
          name: 'ณัฐนิตา นวลสุข',
          tel: '0956789012',
          gender: 'other',
          inDate: new Date('2021-05-10'),
          pointAmount: 28,
          pointRate: 0.05,
        },
        {
          id: 16,
          name: 'ศิริวัฒน์ พรหมสิริ',
          tel: '0967890123',
          gender: 'female',
          inDate: new Date('2021-06-22'),
          pointAmount: 42,
          pointRate: 0.05,
        },
        {
          id: 17,
          name: 'ภัทรพล รักวิวัฒน์',
          tel: '0978901234',
          gender: 'male',
          inDate: new Date('2021-07-07'),
          pointAmount: 24,
          pointRate: 0.05,
        },
        {
          id: 18,
          name: 'ศรัณย์วุฒิ มีดี',
          tel: '0989012345',
          gender: 'other',
          inDate: new Date('2021-08-18'),
          pointAmount: 30,
          pointRate: 0.05,
        },
        {
          id: 19,
          name: 'พลอยชมพู วงศ์สวัสดิ์',
          tel: '0990123456',
          gender: 'female',
          inDate: new Date('2021-09-30'),
          pointAmount: 36,
          pointRate: 0.05,
        },
        {
          id: 20,
          name: 'ธนบดี สุนทรางค์',
          tel: '0101234567',
          gender: 'male',
          inDate: new Date('2021-10-12'),
          pointAmount: 19,
          pointRate: 0.05,
        },
        {
          id: 21,
          name: 'ประพัฒน์ สุขสวัสดิ์',
          tel: '0112345678',
          gender: 'female',
          inDate: new Date('2021-11-25'),
          pointAmount: 28,
          pointRate: 0.05,
        },
        {
          id: 22,
          name: 'สุพัตรา นวลสุข',
          tel: '0123456789',
          gender: 'other',
          inDate: new Date('2021-12-08'),
          pointAmount: 22,
          pointRate: 0.05,
        },
        {
          id: 23,
          name: 'ธนกฤต รักชาติ',
          tel: '0134567890',
          gender: 'male',
          inDate: new Date('2022-01-20'),
          pointAmount: 15,
          pointRate: 0.05,
        },
        {
          id: 24,
          name: 'สุนิสา สุขสวัสดิ์',
          tel: '0145678901',
          gender: 'female',
          inDate: new Date('2022-02-05'),
          pointAmount: 33,
          pointRate: 0.05,
        },
        {
          id: 25,
          name: 'เทพวัฒน์ มีดี',
          tel: '0156789012',
          gender: 'male',
          inDate: new Date('2022-03-15'),
          pointAmount: 25,
          pointRate: 0.05,
        },
        {
          id: 26,
          name: 'ณัฐชนน แสนสุข',
          tel: '0167890123',
          gender: 'other',
          inDate: new Date('2022-04-28'),
          pointAmount: 40,
          pointRate: 0.05,
        },
        {
          id: 27,
          name: 'ปวริศ รักวิวัฒน์',
          tel: '0178901234',
          gender: 'female',
          inDate: new Date('2022-05-10'),
          pointAmount: 28,
          pointRate: 0.05,
        },
        {
          id: 28,
          name: 'ศุภกิจ พรหมสิริ',
          tel: '0189012345',
          gender: 'male',
          inDate: new Date('2022-06-22'),
          pointAmount: 22,
          pointRate: 0.05,
        },
        {
          id: 29,
          name: 'ศุภวิชญ์ พรหมสุข',
          tel: '0190123456',
          gender: 'other',
          inDate: new Date('2022-07-07'),
          pointAmount: 18,
          pointRate: 0.05,
        },
        {
          id: 30,
          name: 'รัชชานนท์ รักวิวัฒน์',
          tel: '0201234567',
          gender: 'female',
          inDate: new Date('2022-08-18'),
          pointAmount: 35,
          pointRate: 0.05,
        },
      ]

  }
  const initialCustomer: Customer = {
    id: -1,
    name: '',
    tel: '',
    gender: 'male',
    inDate: new Date(),
    pointAmount: 0,
    pointRate: 0.05,
  }
  const lastId = customers.value.length
  const currentCustomer = ref<Customer | null>()
  const dialog = ref(false)
  const form = ref(false)
  const loading = ref(false)
  let editedIndex = -1
  const dialogDelete = ref(false)
  const refForm = ref<VForm | null>(null)
  const editedCustomer = ref<Customer>(JSON.parse(JSON.stringify(initialCustomer)))


  const header = [{
    title: 'id',
    key: 'id',
    value: 'id'
  },
  {
    title: 'Full name',
    key: 'name',
    value: 'name'
  },
  {
    title: 'Tel',
    key: 'tel',
    value: 'tel'
  },

  {
    title: 'Gender',
    key: 'gender',
    value: 'gender'
  },

  { title: 'Actions', key: 'actions', sortable: false }

  ]


  const searchMember = (tel: string) => {
    const index = customers.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentCustomer.value = null
    }

    currentCustomer.value = customers.value[index]
  }


  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedCustomer.value = Object.assign({}, initialCustomer)

    })
  }

  function deleteItemConfirm() {
    customers.value.splice(editedIndex, 1)
    closeDelete()
  }

  function editItem(item: Customer) {
    editedIndex = customers.value.indexOf(item)
    editedCustomer.value = Object.assign({}, item)
    dialog.value = true
  }
  function deleteItem(item: Customer) {
    editedIndex = customers.value.indexOf(item)
    editedCustomer.value = Object.assign({}, item)
    dialogDelete.value = true
    editedIndex = -1
  }



  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedCustomer.value = Object.assign({}, initialCustomer)
      editedIndex = -1
    })

  }

  function onSubmit() {

  }

  function clear() {
    currentCustomer.value = null
  }

  return {
    customers, currentCustomer, form, loading, header, dialog, editedCustomer,
    onSubmit,
    deleteItemConfirm,
    clear, searchMember, closeDelete, closeDialog, dialogDelete, initialize, deleteItem, editItem, editedIndex, lastId
  }
})