
import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'

//import store
import { useUserStore } from '../user';


//type
import type { UtilityDetial } from '../../types/utiliycost/UtilityDetail';
import type { UtilityCost } from '../../types/utiliycost/UtilityCost';


const userStore = useUserStore();
const price = ref(0)
const type = ref("")
const newUcDialog = ref(false);

export const useUtilityStore = defineStore('utility ', () => {
    const initUtilityCost = ref<UtilityCost>({
        id: 0,
        creatDate: new Date(),
        total: 0,
        totalItem: 0,
        userID: userStore.currentUser?.id ?? 0,
        utilityDetail: []
    })

    const utilityBills = ref<UtilityCost[]>([







    ])

    const utilityDetails = ref<UtilityDetial[]>([
    ])


    const newItem = (uc: UtilityDetial) => {
        console.log(uc)
        const index = utilityDetails.value.findIndex((item) => item.id === uc.id)
        console.log(index)
        if (index >= 0) {
            return
        } else {
            const newItem: UtilityDetial = {
                id: utilityDetails.value.length + 1,
                type: type.value,
                price: price.value,
            }
            nextTick(() => {
                console.log(newItem)
                utilityDetails.value.push(newItem)
                calBill()
                price.value = 0
                type.value = ""
            })
        }
    }

    function calBill() {
        let total = 0
        let totalItem = 0
        for (const item of utilityDetails.value) {
            console.log(item.price)
            total = Number(total) + Number(item.price)
            totalItem = utilityDetails.value.length
        }
        initUtilityCost.value.totalItem = totalItem
        initUtilityCost.value.total = total
        initUtilityCost.value.utilityDetail = utilityDetails.value

    }

    function saveBill() {
        initUtilityCost.value.id = utilityBills.value.length + 1;
        console.log(initUtilityCost.value)
        utilityBills.value.push(initUtilityCost.value)
        clear()
        
    }

    const deleteItem = (m: UtilityDetial) => {
        console.log("item", m)
        const index = utilityDetails.value.findIndex((item) => item.id === m.id)
        console.log("Item  that find ", utilityDetails.value.find((item) => item.id === item.id))
        console.log("Index", index)
        utilityDetails.value.splice(index, 1)
        calBill()
    }

    function clear() {
        utilityDetails.value = []
        initUtilityCost.value = {
            id: 0,
            creatDate: new Date(),
            total: 0,
            totalItem: 0,
            userID: userStore.currentUser?.id ?? 0,
            utilityDetail: []
            
        }
        newUcDialog.value = false
    }


    function getUserName(userID: any) {
        const user = userStore.users.find((user) => user && user.id === userID);
        return user ? user.fullName : 'Unknown User';
        console.log(user);
    }

    return {
        initUtilityCost, utilityBills, utilityDetails, newItem, deleteItem, calBill, saveBill, getUserName, clear, type, price
        , newUcDialog

    };
})