import { ref, computed, type Ref } from 'vue'
import { defineStore } from 'pinia'

import type { Product } from '@/types/Product'

export const useProductStore = defineStore('product', () => {

  const products = ref<Product[]>([
    { id: 1, name: 'Espresso', price: 45, unit: 30, category: 'drink', subCategory: 'Hot', sweetLevel: '0' },
    { id: 2, name: 'Cappuccino', price: 55, unit: 30, category: 'drink', subCategory: 'Hot', sweetLevel: '25' },
    { id: 3, name: 'Iced Coffee', price: 40, unit: 30, category: 'drink', subCategory: 'Cold', sweetLevel: '50' },
    { id: 4, name: 'Latte', price: 50, unit: 30, category: 'drink', subCategory: 'Hot', sweetLevel: '100' },
    { id: 5, name: 'Green Tea', price: 35, unit: 30, category: 'drink', subCategory: 'Cold', sweetLevel: '0' },
    { id: 6, name: 'Black Tea', price: 30, unit: 30, category: 'drink', subCategory: 'Hot', sweetLevel: '25' },
    { id: 7, name: 'Chocolate', price: 65, unit: 30, category: 'drink', subCategory: 'Frappe', sweetLevel: '50' },
    { id: 8, name: 'Frappe Vanilla', price: 65, unit: 30, category: 'drink', subCategory: 'Frappe', sweetLevel: '100' },
    { id: 9, name: 'Croissant', price: 25, unit: 30, category: 'bakery', subCategory: 'Hot', sweetLevel: '0' },
    { id: 10, name: 'Baguette', price: 20, unit: 30, category: 'bakery', subCategory: 'Cold', sweetLevel: '25' },
    { id: 11, name: 'Blueberry Muffin', price: 30, unit: 30, category: 'bakery', subCategory: 'Hot', sweetLevel: '50' },
    { id: 12, name: 'Chocolate Chip Cookie', price: 15, unit: 30, category: 'bakery', subCategory: 'Cold', sweetLevel: '100' },
    { id: 13, name: 'Spaghetti Bolognese', price: 40, unit: 30, category: 'foods', subCategory: 'Hot', sweetLevel: '0' },
    { id: 14, name: 'Margherita Pizza', price: 50, unit: 30, category: 'foods', subCategory: 'Hot', sweetLevel: '25' },
    { id: 15, name: 'Caesar Salad', price: 35, unit: 30, category: 'foods', subCategory: 'Cold', sweetLevel: '50' },
    { id: 16, name: 'Hamburger', price: 60, unit: 30, category: 'foods', subCategory: 'Hot', sweetLevel: '100' },
    { id: 17, name: 'Fried Rice', price: 45, unit: 30, category: 'foods', subCategory: 'Cold', sweetLevel: '0' },
    { id: 18, name: 'Pad Thai', price: 50, unit: 30, category: 'foods', subCategory: 'Hot', sweetLevel: '25' },
    { id: 19, name: 'Sushi Roll', price: 55, unit: 30, category: 'foods', subCategory: 'Cold', sweetLevel: '50' },
    { id: 20, name: 'Tom Yum Soup', price: 40, unit: 30, category: 'foods', subCategory: 'Hot', sweetLevel: '100' },
    { id: 21, name: 'Iced Chocolate', price: 45, unit: 30, category: 'drink', subCategory: 'Cold', sweetLevel: '0' },
    { id: 22, name: 'Chai Latte', price: 55, unit: 30, category: 'drink', subCategory: 'Hot', sweetLevel: '25' },
    { id: 23, name: 'Mocha', price: 50, unit: 30, category: 'drink', subCategory: 'Frappe', sweetLevel: '50' },
    { id: 24, name: 'Matcha Latte', price: 60, unit: 30, category: 'drink', subCategory: 'Cold', sweetLevel: '100' },
    { id: 25, name: 'Apple Pie', price: 35, unit: 30, category: 'bakery', subCategory: 'Hot', sweetLevel: '0' },
    { id: 26, name: 'Cheese Danish', price: 30, unit: 30, category: 'bakery', subCategory: 'Cold', sweetLevel: '25' },
    { id: 27, name: 'Cinnamon Roll', price: 40, unit: 30, category: 'bakery', subCategory: 'Hot', sweetLevel: '50' },
    { id: 28, name: 'Red Velvet Cupcake', price: 25, unit: 30, category: 'bakery', subCategory: 'Cold', sweetLevel: '100' },
    { id: 29, name: 'Chicken Alfredo Pasta', price: 50, unit: 30, category: 'foods', subCategory: 'Hot', sweetLevel: '0' },
    { id: 30, name: 'Vegetarian Pizza', price: 45, unit: 30, category: 'foods', subCategory: 'Cold', sweetLevel: '25' }
  ])
  function Sortlist(Cate: String) {
    const newlist: Product[] = []
    let a = 0;
    for (let i = 0; i < products.value.length; i++) {
      if (products.value[i].category == Cate) {
        newlist[a] = products.value[i];
        a++
      }

    }
    return newlist
  }

  return { products, Sortlist }
})