import { ref, nextTick } from 'vue';
import { defineStore } from 'pinia'
import type { User } from '@/types/User';

//component
import UploadUserImage from '@/components/UploadUserImage.vue';

export const useUserStore = defineStore('user', () => {
  const dialog = ref(false);
  const dialogDelete = ref(false);
  

  const initiUser: User = {
    id: -1,
    fullName: "",
    email: "",
    tel: "",
    address: "",
    rank: "พนักงาน",
    status: "Active",
    password: "",
    user: "",
  };

  const editedUser = ref<User>(JSON.parse(JSON.stringify(initiUser)));
  let editedIndex = -1;
  const currentUser = ref<User|null>( 
    {
      id: 12,
        fullName: 'โบว์ เวิร์ธ',
        email: 'bow.worth@example.com',
        tel: '0889988776',
        address: '789 ถนนสุขุมวิท, แขวงคลองเตย, เขตคลองเตย, กรุงเทพฯ 10234',
        rank: 'ผู้จัดการ',
        status: 'Active',
        password: 'assistant123',
        user: 'bow'
    },
  )


  
    const users = ref<User[]>([
      {
        id: 1,
        fullName: 'กฤติน ศรสุข',
        email: 'krit@example.com',
        tel: '0881234567',
        address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
        rank: 'พนักงานขาย',
        status: 'Active',
        password: 'pass1234',
        user: 'Krit'
      },
      {
        id: 2,
        fullName: 'สุดใจ มีความสุข',
        email: 'sudjai.meekwamsook@example.com',
        tel: '0887654321',
        address: '456 ถนนโคราช, แขวงดุสิต, เขตดุสิต, กรุงเทพฯ 56789',
        rank: 'ผู้ช่วยฝ่ายบริการลูกค้า',
        status: 'Active',
        password: 'strongpass',
        user: 'sudjai'
      },
      {
        id: 3,
        fullName: 'รพีพัฒน์ ดีใจ',
        email: 'rapiwat.deejai@example.com',
        tel: '0889876543',
        address: '789 ถนนสุขุมวิท, แขวงคลองเตย, เขตคลองเตย, กรุงเทพฯ 10123',
        rank: 'ผู้จัดการฝ่ายขาย',
        status: 'Active',
        password: 'pass1234',
        user: 'rapipat'
      },
      {
        id: 4,
        fullName: 'ณัฐฐิชา พุฒิพงษ์',
        email: 'nattacha.puttipong@example.com',
        tel: '0881122334',
        address: '555 ถนนราชดำเนิน, แขวงพระบรมมหาราชวัง, เขตพระนคร, กรุงเทพฯ 10234',
        rank: 'ผู้จัดการ',
        status: 'Active',
        password: 'happy123',
        user: 'nattacha'
      },
      {
        id: 5,
        fullName: 'ธีระพล ประดิษฐ์',
        email: 'teerapong.pradit@example.com',
        tel: '0889988776',
        address: '999 ถนนสีลม, แขวงสุริยวงศ์, เขตบางรัก, กรุงเทพฯ 54321',
        rank: 'นักวิเคราะห์ธุรกิจ',
        status: 'Active',
        password: 'analyze456',
        user: 'teerapong'
      },
      {
        id: 6,
        fullName: 'อรวรรณ สุขสวัสดิ์',
        email: 'orawan.suksawat@example.com',
        tel: '0887654321',
        address: '123 ถนนเพชรบุรี, แขวงทุ่งมหาเมฆ, เขตราชเทวี, กรุงเทพฯ 67890',
        rank: 'ผู้ช่วยผู้จัดการฝ่ายการตลาด',
        status: 'Active',
        password: 'market789',
        user: 'orawan'
      },
      {
        id: 7,
        fullName: 'พิชาธิป ช่างมัน',
        email: 'pichathip.changman@example.com',
        tel: '0881122334',
        address: '789 ถนนพหลโยธิน, แขวงลาดยาว, เขตจตุจักร, กรุงเทพฯ 34567',
        rank: 'ผู้จัดการ',
        status: 'Active',
        password: 'system4321',
        user: 'pichathip'
      },
      {
        id: 8,
        fullName: 'เจนนิ เดนเวอร์',
        email: 'jenni.denver@example.com',
        tel: '0889988776',
        address: '123 ถนนแสนสุข, แขวงบางจาก, เขตบางกะปิ, กรุงเทพฯ 10123',
        rank: 'พนักงานขาย',
        status: 'Active',
        password: 'salestaff123',
        user: 'jenni'
      },
      {
        id: 9,
        fullName: 'เอ็มมา สวัสดิ์ดี',
        email: 'emma.sawasdee@example.com',
        tel: '0881122334',
        address: '789 ถนนรัชดา, แขวงสะพานสูง, เขตวัฒนา, กรุงเทพฯ 10234',
        rank: 'พนักงานบริการลูกค้า',
        status: 'Active',
        password: 'customer1234',
        user: 'emma'
      },
      {
        id: 10,
        fullName: 'นัทธิพร วงศ์วารี',
        email: 'natthiporn.wongwari@example.com',
        tel: '0889988776',
        address: '999 ถนนพระราม 9, แขวงห้วยขวาง, เขตห้วยขวาง, กรุงเทพฯ 54321',
        rank: 'นักวิเคราะห์ธุรกิจ',
        status: 'Active',
        password: 'business4321',
        user: 'natthiporn'
      },
      {
        id: 11,
        fullName: 'ไอริส แซนเจอร์',
        email: 'iris.sanjor@example.com',
        tel: '0881122334',
        address: '123 ถนนสีลม, แขวงบางรัก, เขตบางรัก, กรุงเทพฯ 10123',
        rank: 'ผู้จัดการ',
        status: 'Active',
        password: 'marketing789',
        user: 'iris'
      },
      {
        id: 12,
        fullName: 'โบว์ เวิร์ธ',
        email: 'bow.worth@example.com',
        tel: '0889988776',
        address: '789 ถนนสุขุมวิท, แขวงคลองเตย, เขตคลองเตย, กรุงเทพฯ 10234',
        rank: 'ผู้จัดการ',
        status: 'Active',
        password: 'assistant123',
        user: 'bow'
      },
      {
        id: 13,
        fullName: 'แอลเล็กซ์ ซันเดอร์',
        email: 'alex.sander@example.com',
        tel: '0881122334',
        address: '555 ถนนราชดำเนิน, แขวงพระบรมมหาราชวัง, เขตพระนคร, กรุงเทพฯ 12345',
        rank: 'พนักงานขาย',
        status: 'Active',
        password: 'sales123',
        user: 'alex'
      },
      {
        id: 14,
        fullName: 'โมจิ มาซากิ',
        email: 'moji.masaki@example.com',
        tel: '0889988776',
        address: '123 ถนนเพชรบุรี, แขวงทุ่งมหาเมฆ, เขตราชเทวี, กรุงเทพฯ 67890',
        rank: 'นักวิเคราะห์ระบบ',
        status: 'Active',
        password: 'system789',
        user: 'moji'
      },
      {
        id: 15,
        fullName: 'แชร์ล็อต สตีเวนสัน',
        email: 'charlotte.stevenson@example.com',
        tel: '0881122334',
        address: '789 ถนนพหลโยธิน, แขวงลาดยาว, เขตจตุจักร, กรุงเทพฯ 34567',
        rank: 'นักวิเคราะห์ธุรกิจ',
        status: 'Active',
        password: 'analyze4321',
        user: 'charlotte'
      }
    ])

    function closeDelete() {
      dialogDelete.value = false;
      nextTick(() => {
        editedUser.value = Object.assign({}, initiUser);
      });
    }
  
    function deleteItemConfirm() {
      users.value.splice(editedIndex, 1);
      closeDelete();
    }
  
    function editItem(item: User) {
      editedIndex = users.value.indexOf(item);
      editedUser.value = Object.assign({}, item);
      dialog.value = true;
    }
  
    function deleteItem(item: User) {
      editedIndex = users.value.indexOf(item);
      editedUser.value = Object.assign({}, item);
      dialogDelete.value = true;
    }

    function initialize() {
      users.value = [
        {
          id: 1,
          fullName: 'กฤติน ศรสุข',
          email: 'krit@example.com',
          tel: '0881234567',
          address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
          rank: 'พนักงานขาย',
          status: 'Active',
          password: 'pass1234',
          user: 'Krit'
        },
        {
          id: 2,
          fullName: 'สุดใจ มีความสุข',
          email: 'sudjai.meekwamsook@example.com',
          tel: '0887654321',
          address: '456 ถนนโคราช, แขวงดุสิต, เขตดุสิต, กรุงเทพฯ 56789',
          rank: 'ผู้ช่วยฝ่ายบริการลูกค้า',
          status: 'Active',
          password: 'strongpass',
          user: 'sudjai'
        },
        {
          id: 3,
          fullName: 'รพีพัฒน์ ดีใจ',
          email: 'rapiwat.deejai@example.com',
          tel: '0889876543',
          address: '789 ถนนสุขุมวิท, แขวงคลองเตย, เขตคลองเตย, กรุงเทพฯ 10123',
          rank: 'ผู้จัดการฝ่ายขาย',
          status: 'Active',
          password: 'pass1234',
          user: 'rapipat'
        },
        {
          id: 4,
          fullName: 'ณัฐฐิชา พุฒิพงษ์',
          email: 'nattacha.puttipong@example.com',
          tel: '0881122334',
          address: '555 ถนนราชดำเนิน, แขวงพระบรมมหาราชวัง, เขตพระนคร, กรุงเทพฯ 10234',
          rank: 'ผู้จัดการ',
          status: 'Active',
          password: 'happy123',
          user: 'nattacha'
        },
        {
          id: 5,
          fullName: 'ธีระพล ประดิษฐ์',
          email: 'teerapong.pradit@example.com',
          tel: '0889988776',
          address: '999 ถนนสีลม, แขวงสุริยวงศ์, เขตบางรัก, กรุงเทพฯ 54321',
          rank: 'นักวิเคราะห์ธุรกิจ',
          status: 'Active',
          password: 'analyze456',
          user: 'teerapong'
        },
        {
          id: 6,
          fullName: 'อรวรรณ สุขสวัสดิ์',
          email: 'orawan.suksawat@example.com',
          tel: '0887654321',
          address: '123 ถนนเพชรบุรี, แขวงทุ่งมหาเมฆ, เขตราชเทวี, กรุงเทพฯ 67890',
          rank: 'ผู้ช่วยผู้จัดการฝ่ายการตลาด',
          status: 'Active',
          password: 'market789',
          user: 'orawan'
        },
        {
          id: 7,
          fullName: 'พิชาธิป ช่างมัน',
          email: 'pichathip.changman@example.com',
          tel: '0881122334',
          address: '789 ถนนพหลโยธิน, แขวงลาดยาว, เขตจตุจักร, กรุงเทพฯ 34567',
          rank: 'ผู้จัดการ',
          status: 'Active',
          password: 'system4321',
          user: 'pichathip'
        },
        {
          id: 8,
          fullName: 'เจนนิ เดนเวอร์',
          email: 'jenni.denver@example.com',
          tel: '0889988776',
          address: '123 ถนนแสนสุข, แขวงบางจาก, เขตบางกะปิ, กรุงเทพฯ 10123',
          rank: 'พนักงานขาย',
          status: 'Active',
          password: 'salestaff123',
          user: 'jenni'
        },
        {
          id: 9,
          fullName: 'เอ็มมา สวัสดิ์ดี',
          email: 'emma.sawasdee@example.com',
          tel: '0881122334',
          address: '789 ถนนรัชดา, แขวงสะพานสูง, เขตวัฒนา, กรุงเทพฯ 10234',
          rank: 'พนักงานบริการลูกค้า',
          status: 'Active',
          password: 'customer1234',
          user: 'emma'
        },
        {
          id: 10,
          fullName: 'นัทธิพร วงศ์วารี',
          email: 'natthiporn.wongwari@example.com',
          tel: '0889988776',
          address: '999 ถนนพระราม 9, แขวงห้วยขวาง, เขตห้วยขวาง, กรุงเทพฯ 54321',
          rank: 'นักวิเคราะห์ธุรกิจ',
          status: 'Active',
          password: 'business4321',
          user: 'natthiporn'
        },
        {
          id: 11,
          fullName: 'ไอริส แซนเจอร์',
          email: 'iris.sanjor@example.com',
          tel: '0881122334',
          address: '123 ถนนสีลม, แขวงบางรัก, เขตบางรัก, กรุงเทพฯ 10123',
          rank: 'ผู้จัดการ',
          status: 'Active',
          password: 'marketing789',
          user: 'iris'
        },
        {
          id: 12,
          fullName: 'โบว์ เวิร์ธ',
          email: 'bow.worth@example.com',
          tel: '0889988776',
          address: '789 ถนนสุขุมวิท, แขวงคลองเตย, เขตคลองเตย, กรุงเทพฯ 10234',
          rank: 'ผู้จัดการ',
          status: 'Active',
          password: 'assistant123',
          user: 'bow'
        },
        {
          id: 13,
          fullName: 'แอลเล็กซ์ ซันเดอร์',
          email: 'alex.sander@example.com',
          tel: '0881122334',
          address: '555 ถนนราชดำเนิน, แขวงพระบรมมหาราชวัง, เขตพระนคร, กรุงเทพฯ 12345',
          rank: 'พนักงานขาย',
          status: 'Active',
          password: 'sales123',
          user: 'alex'
        },
        {
          id: 14,
          fullName: 'โมจิ มาซากิ',
          email: 'moji.masaki@example.com',
          tel: '0889988776',
          address: '123 ถนนเพชรบุรี, แขวงทุ่งมหาเมฆ, เขตราชเทวี, กรุงเทพฯ 67890',
          rank: 'นักวิเคราะห์ระบบ',
          status: 'Active',
          password: 'system789',
          user: 'moji'
        },
        {
          id: 15,
          fullName: 'แชร์ล็อต สตีเวนสัน',
          email: 'charlotte.stevenson@example.com',
          tel: '0881122334',
          address: '789 ถนนพหลโยธิน, แขวงลาดยาว, เขตจตุจักร, กรุงเทพฯ 34567',
          rank: 'นักวิเคราะห์ธุรกิจ',
          status: 'Active',
          password: 'analyze4321',
          user: 'charlotte'
        }
      ];
    
    }
    const author = (user: string, password: string) => {
      const index = users.value.findIndex((item) => item.user === user)
      if (index < 0) {
          currentUser.value = null
      }else{
        console.log(users.value[index].password)
        console.log(password)
        if(users.value[index].password === password){
          currentUser.value = users.value[index]
        }else{
          currentUser.value = null
        }
      }  
    }
    const checkInOutAuthor = (user: string, password: string) => {
      const index = users.value.findIndex((item) => item.user === user)
      if (index < 0) {
        return null
      }else{
        console.log(users.value[index].password)
        console.log(password)
        if(users.value[index].password === password){
          return  users.value[index]
        }else{
          return null
        }
      }  
    }

  function logout() {
    currentUser.value =(null)
  }
    

  return { users,dialog,dialogDelete,initiUser,editedUser,editedIndex,deleteItemConfirm,editItem,deleteItem,closeDelete,initialize,author,currentUser,logout,checkInOutAuthor }
})