import type { StockItem } from '@/types/StockItem';
type Stock = {
    id: number;
    date: Date; 
    userID?: number;
    items?:StockItem[]


};

export type { Stock }; 