import type { Product } from "./Product"

type ReceiptItem = {
    id: number
    name: string
    price: number
    unit: number
    productsId: number
    products?: Product
}

export { type ReceiptItem }