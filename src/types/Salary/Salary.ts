type Status = "PAID" | "NOT PAID"

type Salary = {
  id: number;
  date:Date;
  detail: SalUser[];
};

type SalUser = {
    id: number;
    name: string;
    paymentDate:Date|null;
    status: Status;
};

export type { Salary,SalUser };